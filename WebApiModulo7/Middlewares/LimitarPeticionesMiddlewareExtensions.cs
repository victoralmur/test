﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiModulo7.Contexts;
using WebApiModulo7.DTOs;
using WebApiModulo7.Entidades;

namespace WebApiModulo7.Middlewares
{
    public static class LimitarPeticionesMiddlewareExtensions
    {
        public static IApplicationBuilder UseLimitarPeticiones(this IApplicationBuilder app)
        {
            return app.UseMiddleware<LimitarPeticionesMiddleware>();
        }
    }

    public class LimitarPeticionesMiddleware
    {
        private readonly RequestDelegate siguiente;
        private readonly IConfiguration configuration;

        public LimitarPeticionesMiddleware(RequestDelegate siguiente, IConfiguration configuration)
        {
            this.siguiente = siguiente;
            this.configuration = configuration;
        }

        public async Task InvokeAsync(HttpContext httpContext, ApplicationDbContext context)
        {
            LimitarPeticionesConfiguracion limitarPeticionesConfiguracion = new LimitarPeticionesConfiguracion();
            configuration.GetSection("limitarPeticiones").Bind(limitarPeticionesConfiguracion);

            string ruta = httpContext.Request.Path.ToString();
            bool estaLaRutaEnListaBlanca = limitarPeticionesConfiguracion.ListaBlancaRutas.Any(x => ruta.Contains(x));

            if (estaLaRutaEnListaBlanca)
            {
                await siguiente(httpContext);
                return;
            }

            var llaveStringValues = httpContext.Request.Headers["X-Api-Key"];

            if(llaveStringValues.Count == 0)
            {
                httpContext.Response.StatusCode = 400;
                await httpContext.Response.WriteAsync("Debe proveer la llave en la cabecera X-Api-Key");
                return;
            }

            if(llaveStringValues.Count > 1)
            {
                httpContext.Response.StatusCode = 400;
                await httpContext.Response.WriteAsync("Solo una llave debe estar presente");
                return;
            }

            string llave = llaveStringValues[0];
            LlaveAPI llaveDB = await context.LlavesAPI
                                        .Include(x => x.RestriccionesDominio)
                                        .Include(x => x.RestriccionesIP)
                                        .FirstOrDefaultAsync(x => x.Llave == llave);

            if(llaveDB == null)
            {
                httpContext.Response.StatusCode = 400;
                await httpContext.Response.WriteAsync("La llave no existe");
                return;
            }

            if (!llaveDB.Activa)
            {
                httpContext.Response.StatusCode = 400;
                await httpContext.Response.WriteAsync("La llave se encuentra inactiva");
                return;
            }

            if(llaveDB.TipoLlave == TipoLlave.Gratuita)
            {
                DateTime hoy = DateTime.Today;
                DateTime manana = hoy.AddDays(1);
                var cantidadPeticionesRealizadasHoy = await context.Peticiones.CountAsync( x => x.LlaveId == llaveDB.Id
                                                                                           && x.FechaPeticion >= hoy
                                                                                           && x.FechaPeticion < manana);

                if(cantidadPeticionesRealizadasHoy >= limitarPeticionesConfiguracion.PeticionesPorDiaGratuito)
                {
                    httpContext.Response.StatusCode = 429;  //Too many requests
                    await httpContext.Response.WriteAsync("Ha excedido el limite de peticiones por dia. Si desea " + 
                                                          "realizar mas peticiones, " +
                                                          "actualice su suscripcion a una cuenta profesional");
                    return;
                }
            }

            bool superaRestricciones = PeticionSuperaAlgunaDeLasRestricciones(llaveDB, httpContext);

            if (!superaRestricciones)
            {
                httpContext.Response.StatusCode = 403;
                return;
            }

            Peticion peticion = new Peticion() 
            { 
                LlaveId = llaveDB.Id,
                FechaPeticion = DateTime.UtcNow
            };

            context.Add(peticion);
            await context.SaveChangesAsync();

            await siguiente(httpContext);
        }

        private bool PeticionSuperaAlgunaDeLasRestricciones(LlaveAPI llaveAPI, HttpContext httpContext)
        {
            bool hayRestricciones = llaveAPI.RestriccionesDominio?.Any() == true || llaveAPI.RestriccionesIP?.Any() == true;

            if (!hayRestricciones)
            {
                return true;
            }

            bool peticionSuperaLasRestriccionesDeDominio = PeticionSuperaLasRestriccionesDeDominio(llaveAPI.RestriccionesDominio, httpContext);

            bool peticionSuperaLasRestriccionesDeIP = PeticionSuperaLasRestriccionesDeIP(llaveAPI.RestriccionesIP, httpContext);

            return peticionSuperaLasRestriccionesDeDominio || peticionSuperaLasRestriccionesDeIP;
        }

        private bool PeticionSuperaLasRestriccionesDeIP(List<RestriccionIP> restricciones, HttpContext httpContext)
        {
            if (restricciones == null || restricciones.Count == 0)
            {
                return false;
            }

            string IP = httpContext.Connection.RemoteIpAddress.ToString();

            if(IP == string.Empty)
            {
                return false;
            }

            var superaRestriccion = restricciones.Any(x => x.IP == IP);
            return superaRestriccion;
        }

        private bool PeticionSuperaLasRestriccionesDeDominio(List<RestriccionDominio> restricciones, HttpContext httpContext)
        {
            if(restricciones == null || restricciones.Count == 0)
            {
                return false;
            }

            string referer = httpContext.Request.Headers["Referer"].ToString();

            if(referer == string.Empty)
            {
                return false;
            }

            Uri myUri = new Uri(referer);
            string host = myUri.Host;

            var superaRestriccion = restricciones.Any(x => x.Dominio == host);
            return superaRestriccion;

        }
    }
}
