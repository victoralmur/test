﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiModulo7.DTOs
{
    public class LlaveDTO
    {
        public int Id { get; set; }
        public string Llave { get; set; }
        public bool Activa { get; set; }
        public string TipoLlave { get; set; }
        public int MyProperty { get; set; }
        public List<RestriccionDominioDTO> RestriccionesDominio { get; set; }
        public List<RestriccionIPDTO> RestriccionesIP { get; set; }
    }
}
