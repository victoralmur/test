﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiModulo7.DTOs
{
    public class ActualizarRestriccionIPDTO
    {
        [Required]
        public string IP { get; set; }

    }
}
