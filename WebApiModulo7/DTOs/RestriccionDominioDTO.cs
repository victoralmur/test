﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiModulo7.DTOs
{
    public class RestriccionDominioDTO
    {
        public int Id { get; set; }
        public string Dominio { get; set; }
    }
}
